package com.atexo.equalshashcodeexample.entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

/**
 * Cette classe permet de démontrer comment implementer efficacement les
 * méthodes equals et hashCode en utilisant une clé primaire autogénérée.
 *
 * @author nicolas Audéon (nicolas.audeon@atexo.com)
 */
@Entity
@Table(name = "PRODUCTS")
@RequiredArgsConstructor
public class Product {

    protected Product() {
        super();
        this.code = UUID.randomUUID().toString();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    @Getter
    @Setter
    private Long id;

    @Version
    @Getter
    @Column(name = "VERSION", nullable = false)
    private int version;

    @NaturalId
    @Getter
    @Column(name = "CODE", nullable = false, length = 50)
    private final String code;

    @Getter
    @Setter
    @Column(name = "LABEL", nullable = true)
    private String label;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (null == obj || Hibernate.getClass(this) != Hibernate.getClass(obj)) {
            return false;
        }
        final Product category = (Product) obj;
        return null != this.getId() && Objects.equals(this.getId(), category.getId());
    }

    @Override
    public int hashCode() {
        return this.getClass().hashCode();
    }

}

