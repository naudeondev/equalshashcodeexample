package com.atexo.equalshashcodeexample.entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.Objects;

/**
 * Cette classe permet de démontrer comment implementer efficacement les
 * méthodes equals et hashCode en utilisant une clé naturelle immutable.
 *
 * @author nicolas Audéon (nicolas.audeon@atexo.com)
 */
@Entity
@Table(name = "CATEGORIE")
@RequiredArgsConstructor
public class Category {

    protected Category() {
        super();
        this.code = "";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    @Getter
    private Long id;

    @Version
    @Getter
    @Column(name = "VERSION", nullable = false)
    private int version;

    @NaturalId
    @Getter
    @Column(name = "CODE", nullable = false, length = 50)
    private final String code;

    @Getter
    @Setter
    @Column(name = "LABEL", nullable = true)
    private String label;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (null == obj || Hibernate.getClass(this) != Hibernate.getClass(obj)) {
            return false;
        }
        final Category category = (Category) obj;
        return null != this.getCode() && Objects.equals(this.getCode(), category.getCode());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getCode());
    }
}
