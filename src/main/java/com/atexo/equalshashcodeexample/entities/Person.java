package com.atexo.equalshashcodeexample.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

/**
 * Cette classe permet de démontrer comment implementer efficacement les
 * méthodes equals et hashCode en utilisant le plugin JPA Buddy.
 *
 * @author nicolas Audéon (nicolas.audeon@atexo.com)
 */
@Entity
@Table(name = "PEOPLE")
public class Person {

    protected Person() {
        super();
    }

    public Person(final String firstName, final String lastName) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    @Getter
    @Setter
    private Long id;

    @Version
    @Getter
    @Column(name = "VERSION", nullable = false)
    private int version;

    @Getter
    @Setter
    @Column(name = "FIRST_NAME", nullable = false, length = 50)
    private String firstName;

    @Getter
    @Setter
    @Column(name = "LAST_NAME", nullable = false, length = 50)
    private String lastName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Person person = (Person) o;
        return this.getId() != null && Objects.equals(this.getId(), person.getId());
    }

    @Override
    public int hashCode() {
        return 0;
    }

}

