package com.atexo.equalshashcodeexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EqualsHashCodeExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(EqualsHashCodeExampleApplication.class, args);
    }

}
