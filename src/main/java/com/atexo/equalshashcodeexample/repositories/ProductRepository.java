package com.atexo.equalshashcodeexample.repositories;

import com.atexo.equalshashcodeexample.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author nicolas Audéon (nicolas.audeon@atexo.com)
 */
public interface ProductRepository extends JpaRepository<Product, Long> {
}
