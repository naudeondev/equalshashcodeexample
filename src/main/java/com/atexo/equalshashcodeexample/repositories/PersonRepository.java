package com.atexo.equalshashcodeexample.repositories;

import com.atexo.equalshashcodeexample.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author nicolas Audéon (nicolas.audeon@atexo.com)
 */
public interface PersonRepository  extends JpaRepository<Person, Long> {
}
