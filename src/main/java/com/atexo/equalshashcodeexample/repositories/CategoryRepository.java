package com.atexo.equalshashcodeexample.repositories;

import com.atexo.equalshashcodeexample.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author nicolas Audéon (nicolas.audeon@atexo.com)
 */
public interface CategoryRepository extends JpaRepository<Category, Long> {

    Optional<Category> findByCode(final String pCode);
}
