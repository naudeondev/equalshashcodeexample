package com.atexo.equalshashcodeexample.entities;

import com.atexo.equalshashcodeexample.repositories.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Classe de test des méthodes equals et hashCode utilisant
 * une clé naturelle immutable telles qu'elles sont implémentées dans
 * l'entité {@link Category}.
 *
 * @author nicolas Audéon (nicolas.audeon@atexo.com)
 */
@DataJpaTest
public class CategoryTests {

    @Autowired
    private CategoryRepository repository;

    @Autowired
    private EntityManager em;

    // **** Test l'implémentation de la méthode equals ****

    @Test
    void category_equals_implementation_should_respect_equals_contract() {

        // Given
        final Category category1 = new Category();
        final Category category2 = new Category();
        final Category category3 = new Category();

        // When

        // Then
        // Rule 1 : It is reflexive: for any non-null reference value x, x.equals(x)
        // should return true.
        assertThat(category1.equals(category1)).isTrue();
        assertThat(category2.equals(category2)).isTrue();
        assertThat(category3.equals(category3)).isTrue();
        // Rule 2 : It is symmetric: for any non-null reference values x and y, x.equals(y)
        // should return true if and only if y.equals(x) returns true.
        assertThat(category1.equals(category2)).isTrue();
        assertThat(category2.equals(category1)).isTrue();
        // Rule 3 : It is transitive: for any non-null reference values x, y, and z, if x.equals(y)
        // returns true and y.equals(z) returns true, then x.equals(z) should return true.
        assertThat(category1.equals(category2)).isTrue();
        assertThat(category2.equals(category3)).isTrue();
        assertThat(category1.equals(category3)).isTrue();
        // Rule 4 : It is consistent: for any non-null reference values x and y, multiple invocations of x.equals(y)
        // consistently return true or consistently return false, provided no information used in equals comparisons
        // on the objects is modified.
        assertThat(category1.equals(category1)).isTrue();
        assertThat(category1.equals(category1)).isTrue();
        assertThat(category1.equals(category1)).isTrue();
        // Rule 5 : For any non-null reference value x, x.equals(null) should return false.
        assertThat(category1.equals(null)).isFalse();
        assertThat(category2.equals(null)).isFalse();
        assertThat(category3.equals(null)).isFalse();
    }

    // **** Test l'implémentation de la méthode hashCode ****

    @Test
    void category_hashCode_implementation_should_respect_hashCode_contract() {

        // Given
        final Category category1 = new Category();
        final Category category2 = new Category();

        // When

        // Then
        // Rule 1 : Whenever it is invoked on the same object more than once during an execution of a Java application,
        // the hashCode method must consistently return the same integer, provided no information used in equals
        // comparisons on the object is modified. This integer need not remain consistent from one execution of an
        // application to another execution of the same application.
        final int hasCode1 = category1.hashCode();
        try {
            Thread.sleep(30L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        final int hasCode2 = category1.hashCode();
        assertThat(hasCode1 == hasCode2).isTrue();
        // Rule 2 : If two objects are equal according to the equals(Object) method, then calling the hashCode method
        // on each of the two objects must produce the same integer result.
        assertThat(category1.hashCode() == category2.hashCode()).isTrue();

    }

    // **** Test le fonctionnement des collections (HashSet et HashMap) ****

    @Test
    void given_two_categories_with_the_same_code_when_adding_first_to_a_set_then_call_contains_on_its_with_second_category_return_true() {

        // Given
        final Category category1 = new Category("1");
        final Category category2 = new Category("1");

        // When
        final Set<Category> categories = new HashSet<>();
        categories.add(category1);

        // Then
        assertThat(categories.iterator().next()).isEqualTo(category2);
        assertThat(categories.contains(category2)).isTrue();
    }

    @Test
    void given_two_categories_with_the_same_code_when_adding_first_to_a_map_then_call_containsEntry_on_its_with_second_category_return_true() {

        // Given
        final Category category1 = new Category("1");
        final Category category2 = new Category("1");

        // When
        final Map<Category, String> map = new HashMap<>();
        map.put(category1, "value");

        // Then
        assertThat(map).hasSize(1);
        assertThat(map.keySet().iterator().next()).isEqualTo(category2);
        assertThat(map.values().iterator().next()).isEqualTo("value");
        assertThat(map).containsEntry(category2, "value");
    }

    @Test
    void set_should_not_contain_doubles() {

        // Given
        final Category category1 = new Category("1");
        final Category category2 = new Category("1");

        // When
        final Set<Category> categories = new HashSet<>();
        categories.add(category1);
        categories.add(category2);

        // Then
        assertThat(category1.equals(category2)).isTrue();
        assertThat(categories).hasSize(1);
    }

    // **** Tests spécifiques pour hibernate ****

    @Test
    void two_transient_categories_need_to_be_not_equal() {

        // Given
        final Category category1 = new Category("1");
        final Category category2 = new Category("2");

        // When

        // Then
        assertThat(category1.equals(category2)).isFalse();
    }

    @Test
    @Transactional
    void two_managed_categories_that_represent_different_records_need_to_be_not_equal() {

        // Given
        this.repository.save(new Category("1"));
        this.repository.save(new Category("2"));

        // When
        final Category category1 = this.repository.findByCode("1").orElseThrow();
        final Category category2 = this.repository.findByCode("2").orElseThrow();

        // Then
        assertThat(category1.equals(category2)).isFalse();
    }

    @Test
    @Transactional
    void two_managed_categories_that_represent_the_same_record_need_to_be_equal() {

        // Given
        this.repository.save(new Category("1"));

        // When
        final Category category1 = this.repository.findByCode("1").orElseThrow();
        final Category category2 = this.repository.findByCode("1").orElseThrow();

        // Then
        assertThat(category1.equals(category2)).isTrue();
    }

    @Test
    @Transactional
    void a_detached_and_a_managed_category_that_represent_the_same_record_need_to_be_equal() {

        // Given
        this.repository.save(new Category("1"));

        // When
        final Category category1 = this.repository.findByCode("1").orElseThrow();
        this.em.detach(category1);
        final Category category2 = this.repository.findByCode("1").orElseThrow();

        // Then
        assertThat(category1.equals(category2)).isTrue();

    }

    @Test
    @Transactional
    void a_re_attached_and_a_managed_category_that_represent_the_same_record_need_to_be_equal() {

        // Given
        this.repository.save(new Category("1"));

        // When
        Category category1 = this.repository.findByCode("1").orElseThrow();
        this.em.detach(category1);
        category1 = this.em.merge(category1);
        final Category category2 = this.repository.findByCode("1").orElseThrow();

        // Then
        assertThat(category1.equals(category2)).isTrue();

    }

}
