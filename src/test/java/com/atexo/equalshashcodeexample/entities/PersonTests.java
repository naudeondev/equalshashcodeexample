package com.atexo.equalshashcodeexample.entities;

import com.atexo.equalshashcodeexample.repositories.PersonRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Classe de test des méthodes equals et hashCode générée par le plugin JPA Buddy
 * telles qu'elles sont implémentées dans l'entité {@link Person}.
 *
 * @author nicolas Audéon (nicolas.audeon@atexo.com)
 */
@DataJpaTest
public class PersonTests {

    @Autowired
    private PersonRepository repository;

    @Autowired
    private EntityManager em;

    // **** Test l'implémentation de la méthode equals ****

    @Test
    void person_equals_implementation_should_respect_equals_contract() {

        // Given
        final Person person1 = new Person();
        person1.setId(1L);
        final Person person2 = new Person();
        person2.setId(1L);
        final Person person3 = new Person();
        person3.setId(1L);

        // When

        // Then
        // Rule 1 : It is reflexive: for any non-null reference value x, x.equals(x)
        // should return true.
        assertThat(person1.equals(person1)).isTrue();
        assertThat(person2.equals(person2)).isTrue();
        assertThat(person3.equals(person3)).isTrue();
        // Rule 2 : It is symmetric: for any non-null reference values x and y, x.equals(y)
        // should return true if and only if y.equals(x) returns true.
        assertThat(person1.equals(person2)).isTrue();
        assertThat(person2.equals(person1)).isTrue();
        // Rule 3 : It is transitive: for any non-null reference values x, y, and z, if x.equals(y)
        // returns true and y.equals(z) returns true, then x.equals(z) should return true.
        assertThat(person1.equals(person2)).isTrue();
        assertThat(person2.equals(person3)).isTrue();
        assertThat(person1.equals(person3)).isTrue();
        // Rule 4 : It is consistent: for any non-null reference values x and y, multiple invocations of x.equals(y)
        // consistently return true or consistently return false, provided no information used in equals comparisons
        // on the objects is modified.
        assertThat(person1.equals(person1)).isTrue();
        assertThat(person1.equals(person1)).isTrue();
        assertThat(person1.equals(person1)).isTrue();
        // Rule 5 : For any non-null reference value x, x.equals(null) should return false.
        assertThat(person1.equals(null)).isFalse();
        assertThat(person2.equals(null)).isFalse();
        assertThat(person3.equals(null)).isFalse();
    }

    // **** Test l'implémentation de la méthode hashCode ****

    @Test
    void person_hashCode_implementation_should_respect_hashCode_contract() {

        // Given
        final Person person1 = new Person();
        person1.setId(1L);
        final Person person2 = new Person();
        person2.setId(1L);

        // When

        // Then
        // Rule 1 : Whenever it is invoked on the same object more than once during an execution of a Java application,
        // the hashCode method must consistently return the same integer, provided no information used in equals
        // comparisons on the object is modified. This integer need not remain consistent from one execution of an
        // application to another execution of the same application.
        final int hasCode1 = person1.hashCode();
        try {
            Thread.sleep(30L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        final int hasCode2 = person1.hashCode();
        assertThat(hasCode1 == hasCode2).isTrue();
        // Rule 2 : If two objects are equal according to the equals(Object) method, then calling the hashCode method
        // on each of the two objects must produce the same integer result.
        assertThat(person1.hashCode() == person2.hashCode()).isTrue();

    }

    // **** Test le fonctionnement des collections (HashSet et HashMap) ****

    @Test
    void given_two_people_with_the_same_code_when_adding_first_to_a_set_then_call_contains_on_its_with_second_person_return_true() {

        // Given
        final Person person1 = new Person();
        person1.setId(1L);
        final Person person2 = new Person();
        person2.setId(1L);

        // When
        final Set<Person> people = new HashSet<>();
        people.add(person1);

        // Then
        assertThat(people.iterator().next()).isEqualTo(person2);
        assertThat(people.contains(person2)).isTrue();
    }

    @Test
    void given_two_people_with_the_same_code_when_adding_first_to_a_map_then_call_containsEntry_on_its_with_second_person_return_true() {

        // Given
        final Person person1 = new Person();
        person1.setId(1L);
        final Person person2 = new Person();
        person2.setId(1L);

        // When
        final Map<Person, String> map = new HashMap<>();
        map.put(person1, "value");

        // Then
        assertThat(map).hasSize(1);
        assertThat(map.keySet().iterator().next()).isEqualTo(person2);
        assertThat(map.values().iterator().next()).isEqualTo("value");
        assertThat(map).containsEntry(person2, "value");
    }

    @Test
    void set_should_not_contain_doubles() {

        // Given
        final Person person1 = new Person();
        person1.setId(1L);
        final Person person2 = new Person();
        person2.setId(1L);

        // When
        final Set<Person> people = new HashSet<>();
        people.add(person1);
        people.add(person2);

        // Then
        assertThat(person1.equals(person2)).isTrue();
        assertThat(people).hasSize(1);
    }

    // **** Tests spécifiques pour hibernate ****

    @Test
    void two_transient_people_need_to_be_not_equal() {

        // Given
        final Person person1 = new Person();
        person1.setId(1L);
        final Person person2 = new Person();
        person2.setId(2L);

        // When

        // Then
        assertThat(person1.equals(person2)).isFalse();
    }

    @Test
    @Transactional
    void two_managed_people_that_represent_different_records_need_to_be_not_equal() {

        // Given
        final long id1 = this.repository.save(new Person("Jean", "Paul")).getId();
        final long id2 = this.repository.save(new Person("Pierre", "Marie")).getId();

        // When
        final Person person1 = this.repository.getById(id1);
        final Person person2 = this.repository.getById(id2);

        // Then
        assertThat(person1.equals(person2)).isFalse();
    }

    @Test
    @Transactional
    void two_managed_people_that_represent_the_same_record_need_to_be_equal() {

        // Given
        final long id = this.repository.save(new Person("Jean", "Paul")).getId();

        // When
        final Person person1 = this.repository.getById(id);
        final Person person2 = this.repository.getById(id);

        // Then
        assertThat(person1.equals(person2)).isTrue();
    }

    @Test
    @Transactional
    void a_detached_and_a_managed_person_that_represent_the_same_record_need_to_be_equal() {

        // Given
        final long id = this.repository.save(new Person("Jean", "Paul")).getId();

        // When
        final Person person1 = this.repository.getById(id);
        this.em.detach(person1);
        final Person person2 = this.repository.getById(id);

        // Then
        assertThat(person1.equals(person2)).isTrue();

    }

    @Test
    @Transactional
    void a_re_attached_and_a_managed_person_that_represent_the_same_record_need_to_be_equal() {

        // Given
        final long id = this.repository.save(new Person("Jean", "Paul")).getId();

        // When
        Person person1 = this.repository.getById(id);
        this.em.detach(person1);
        person1 = this.em.merge(person1);
        final Person person2 = this.repository.getById(id);

        // Then
        assertThat(person1.equals(person2)).isTrue();

    }

}

